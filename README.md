# IPFS Pinning Service for Open Climate Research Data

<img src="https://www.dkrz.de/@@site-logo/dkrz.svg" alt="drawing" width="300"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/1/18/Ipfs-logo-1024-ice-text.png" alt="drawing" width="150"/>

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.dkrz.de%2Fdata-infrastructure-services%2Fipfs-pinning-service-for-open-climate-research-data/d887b466c5affb767c29b5a16251f0fd576870a4?urlpath=lab%2Ftree%2Fhow_to_open_ipfs_file.ipynb)

## Abstract - What is new?
The InterPlanetary File System (IPFS) is a novel decentralized file storage network that allows users to store and share files in a distributed manner, which can make it more resilient if individual infrastructure components fail. It also allows for faster access to content as users can get files directly from other users instead of having to go through a central server. However, one of the challenges of using IPFS is ensuring that the files remain available over time. This is where an IPFS pinning service offers a solution. An IPFS pinning service is a type of service that allows users to store and maintain the availability of their files on the IPFS network. The goal of an IPFS pinning service is to provide a reliable and trusted way for users to ensure that their files remain accessible on the IPFS network. This is accomplished by maintaining a copy of the file on the service's own storage infrastructure, which is then pinned to the IPFS network. This allows users to access the file even if the original source becomes unavailable.

We explored the use of the IPFS for scientific data with a focus on climate data. We set up an IPFS node running on a cloud instance at the German Climate Computing Center where selected scientists can pin their data and make them accessible to the public via the IPFS infrastructure. IPFS is a good choice for climate data, because the open network architecture strengthens open science efforts and enables FAIR data processing workflows. Data within the IPFS is freely accessible to scientists regardless of their location and offers fast access rates to large files. In addition, data within the IPFS is immutable, which ensures that the content of a content identifier does not change over time. Due to the recent development of the IPFS, the project outcomes are novel data science developments for the earth system science and are potentially relevant building blocks to be included in the earth system science community.


## Repository Structure
This repositiory contains:
- General information about the IPFS and [installation and implementation details](https://gitlab.dkrz.de/data-infrastructure-services/ipfs-pinning-service-for-open-climate-research-data/-/blob/main/Documents/Installation_and_Implementation.md)
- A [user guide](https://gitlab.dkrz.de/data-infrastructure-services/ipfs-pinning-service-for-open-climate-research-data/-/blob/main/Documents/How_to_Use.md) with an [example notebook](https://gitlab.dkrz.de/data-infrastructure-services/ipfs-pinning-service-for-open-climate-research-data/-/blob/main/Notebook/how_to_open_ipfs_file.ipynb), which illustrates the use of the service
- Supplementary videos, which explain the project and the use of the service ([Science Talk](https://youtu.be/scByz8dYzio), [Application Tutorial](https://youtu.be/jDrBFgUi7Oc))
